echo Ram : $( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $8}')  / $( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $7}')
echo Swap : $( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $14}')  / $( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $13}')
echo CPU : `top -bn1 | sed -n '/Cpu/p' | awk '{print $2}' ` %
echo Net : `cat /proc/$1/net/netstat | grep 'IpExt: ' | tail -n 1 | awk '{ print  int($8/10^6)" | " int($9/10^6) " mb/s" }'`



# CPU + NET
echo CPU : `top -bn1 | sed -n '/Cpu/p' | awk '{print $2}' ` '% -- Net : '`cat /proc/$1/net/netstat | grep 'IpExt: ' | tail -n 1 | awk '{ print  int($8/10^6)" | " int($9/10^6) " mb/s" }'`
# NET + SWAP
echo Net : `cat /proc/$1/net/netstat | grep 'IpExt: ' | tail -n 1 | awk '{ print  int($8/10^6)" | " int($9/10^6) " mb/s" }'`'% -- Swap  : '$( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $14}')  / $( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $13}')
# NET + RAM
echo Net : `cat /proc/$1/net/netstat | grep 'IpExt: ' | tail -n 1 | awk '{ print  int($8/10^6)" | " int($9/10^6) " mb/s" }'`'% -- Ram: '$( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $8}')  / $( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $7}')
# CPU % + SWAP 
echo CPU : `top -bn1 | sed -n '/Cpu/p' | awk '{print $2}' ` '% -- Swap  : '$( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $14}')  / $( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $13}')
# CPU % + RAM
echo CPU : `top -bn1 | sed -n '/Cpu/p' | awk '{print $2}' ` '% -- Ram: '$( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $8}')  / $( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $7}')
# RAM + SWAP
echo Ram: $( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $8}')  / $( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $7}') ' -- Swap : '$( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $14}')  / $( echo $(free -m | cut -d':' -f2) | /usr/bin/awk -F' '  '{print $13}')