# Make Cinnamon Great Again !

So, if you want to feel the CINNAMON's power just look at this :  

LOOK AT THIS ![MAGNIFIK](https://gitlab.com/CERI_Raymondaud.Q/make-cinnamon-great-again/raw/master/MENUBAR.png "CINNAMON BYOTIFUL DASHBOARD")  

* RIGHT CLICK ON CINNAMON MENUBAR AND LCLICK ON APPLETS
![MAGNIFIK](https://gitlab.com/CERI_Raymondaud.Q/make-cinnamon-great-again/raw/master/1.png "RIGHT CLICK ON CINNAMON MENUBAR AND LCLICK ON APPLETS")  

* GO TO DOWNLOAD AND DOWNLOAD BASHSENSORS
![MANIGIFK](https://gitlab.com/CERI_Raymondaud.Q/make-cinnamon-great-again/raw/master/2.png "GO TO DOWNLOAD AND DOWNLOAD BASHSENSORS")

* GO TO MANAGE AND ADD IT TO THE MENUBAR
![MANIGIFK](https://gitlab.com/CERI_Raymondaud.Q/make-cinnamon-great-again/raw/master/3.png "GO TO MANAGE AND DD IT TO THE MENUBAR")

* CONFIG IT :  You can use commands provided in [bash_sensors_commands.sh](https://gitlab.com/CERI_Raymondaud.Q/make-cinnamon-great-again/-/blob/master/bash_sensor_commands.sh "bash_sensor_commands.sh")
![MANIGIFK](https://gitlab.com/CERI_Raymondaud.Q/make-cinnamon-great-again/raw/master/4.png "CONFIG IT :  You can used commands provided in bash_senso_commands.sh") 

![MANIGIFK](https://gitlab.com/CERI_Raymondaud.Q/make-cinnamon-great-again/raw/master/5.png "CONFIG IT :  You can used commands provided in bash_senso_commands.sh")

* Here is a custom prompt provided in .bashrc

![PROMPT](https://gitlab.com/CERI_Raymondaud.Q/make-cinnamon-great-again/raw/master/Prompt.png "You can take my custom prompt in .bashrc")